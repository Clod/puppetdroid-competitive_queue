__author__ = 'claudio'

from distutils.core import setup

setup(name='queue_mongo',
      version='1.0',
      author='Claudio Rizzo',
      author_email='claudio.rizzo.90@gmail.com',
      py_modules=['queue_mongo'],
      )
