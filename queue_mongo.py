__author__ = 'claudio'

import logging
import datetime


from mongoengine import *

from mongoengine import QuerySet
from mongoengine import OperationError
from mongoengine.queryset import transform



def connect_to_database(database_name, server_address='localhost', port=27017):
    return connect(database_name, host=server_address, port=port)
    #logger = logging.getLogger(__name__)


class BetterQuerySet(QuerySet):
    def and_modify(self, upsert=False, sort=None, full_response=False, **update):
        """
        Stolen from
        https://github.com/bewt85/mongoengine/
        """
        if not update and not upsert:
            raise OperationError(
                "No update parameters, must either update or remove")

        queryset = self.clone()
        query = queryset._query
        update = transform.update(queryset._document, **update)

        try:
            result = queryset._collection.find_and_modify(
                query, update, upsert=upsert,
                sort=sort, full_response=full_response)
            if full_response:
                if not result['value'] is None:
                    result['value'] = self._document._from_son(result['value'])
            else:
                if not result is None:
                    result = self._document._from_son(result)
            return result

        except pymongo.errors.OperationFailure, err:
            raise OperationError(u'findAndModify failed (%s)' % unicode(err))


class WorkManager(BetterQuerySet):
    #worker take a job and poll the queue
    def poll_queue(self, worker_ip):
        job = self.filter(junk=False,
                          done=False,
                          in_progress=False).order_by('time_stamp').and_modify(set__worker_ip=worker_ip,
                                                                               set__dequeued=datetime.datetime.now(),
                                                                               set__in_progress=True)

        if job is not None:
            yield job


    def set_junk(self, client_ip):
       self.filter(client_ip=client_ip).and_modify(set__junk=True)

    #Return true if a worker pulled a job from the queue, false otherwise
    def get_in_progress(self, client_ip):
        job = self.filter(client_ip=client_ip,
                          done=False,
                          junk=False,
                          in_progress=True).and_modify(
                          set__done=True)

        if job is not None:
            yield job

        """
        job = Work(....).save()

        ...

        for j in Work.objects.get_in_progress(pk=job.id):
            assert j is not None
        """

    def push_job(self, client_ip, client_port):
        work = Work(enqueued=datetime.datetime.now,
                    dequeued=datetime.datetime.now,
                    client_ip=client_ip,
                    client_port=client_port,
                    in_progress=False,
                    done=False,
                    junk=False,
                    worker_ip=None)
        work.save()

class Work(Document):
    enqueued = DateTimeField(default=datetime.datetime.now, required=True)
    dequeued = DateTimeField(default=datetime.datetime.now, required=True)
    client_ip = StringField(required=True, primary_key=True)
    client_port = IntField(required=True)
    in_progress = BooleanField(required=True)
    done = BooleanField(required=True)
    junk = BooleanField(required=True)
    worker_ip = StringField()

    meta = {
        'queryset_class': WorkManager, # Work.objects == WorkManager()
        'indexes': [
            ('enqueued',
             'done',
             'in_progress',),

            'done',
        ]
    }
